#Arduino Memory Game

An Arduino UNO based memory game which has its roots from the popular 'Simon Says' gameplay

## Contents

1. Software Programming
2. Hardware Interconnections

### Software Programming
- Files are stored in the /Software directory
- The software programming was done using the Visual Micro plugin for Visual Studio.
- Although these files may not be directly compilable using the Arduino IDE currently, Arduino friendly versions will be added in the future.

### Hardware Programming
- This section will describe the hardware interconnections between the Arduino UNO board and the external chips and pheripherals.
- Circuit diagram [Fritzing] will be uploaded in the future

###Note: This readme is not comprehensive and requires updating.
