// 
// 
// 

#include "OutputStream.h"

/*
**************************************************************************************************
*                                          INITIALIZATION                                        *
**************************************************************************************************
*/
void OutputStream::init(uint8_t latchPin, uint8_t clockPin, uint8_t dataPin)
{
	this->latchPin = latchPin;
	this->clockPin = clockPin;
	this->dataPin = dataPin;
	reset();
}

/*
**************************************************************************************************
*                                        PRIVATE FUNCTIONS                                       *
**************************************************************************************************
*/

/* Shiftout Kernel (Sends 8 bits, MSB first) */
void OutputStream::shiftOut(byte data)
{
	// Initialize
	int i = 0;
	uint8_t pinState;

	// Clear data nd clock lines
	digitalWrite(dataPin, 0);
	digitalWrite(clockPin, 0);

	// Send each bit
	for (i = 7; i >= 0; i--)
	{
		digitalWrite(clockPin, 0);

		if (data & (1 << i))
			pinState = 1;
		else
			pinState = 0;

		// Sets the pin to HIGH or LOW depending on pinState
		digitalWrite(dataPin, pinState);
		// Register shifts bits on upstroke of clock pin  
		digitalWrite(clockPin, 1);
		// Zero the data pin after shift to prevent bleed through
		digitalWrite(dataPin, 0);
	}

	// End shifting
	digitalWrite(clockPin, 0);
}

/* Updates the Output Pins using the passed byte array */
void OutputStream::setOutputPins()
{
	// Set Active-LOW latchPin to start latching
	digitalWrite(latchPin, LOW);
	// Send data starting from Last Byte
	shiftOut(outputBytes[_7SEGMENT_1]);
	shiftOut(outputBytes[_7SEGMENT_2]);
	shiftOut(outputBytes[_LED9]);
	shiftOut(outputBytes[_BLUE_LEDs]);
	shiftOut(outputBytes[_GREEN_LEDs]);
	shiftOut(outputBytes[_RED_LEDs]);
	// Stop latching
	digitalWrite(latchPin, HIGH);
}

/*
**************************************************************************************************
*                                        PUBLIC FUNCTIONS                                        *
**************************************************************************************************
*/

/* Resets the LEDs to Null State */
void OutputStream::reset()
{
	// Set pinmode for OutputStream pins
	pinMode(latchPin, OUTPUT);
	pinMode(clockPin, OUTPUT);
	pinMode(dataPin, OUTPUT);

	// Reset placeholders for output pins
	outputBytes[_RED_LEDs] = B00000000;
	outputBytes[_GREEN_LEDs] = B00000000;
	outputBytes[_BLUE_LEDs] = B00000000;
	outputBytes[_LED9] = B00000000;
	outputBytes[_7SEGMENT_1] = B00000000;
	outputBytes[_7SEGMENT_2] = B00000000;

	// Update the output
	setOutputPins();
}

/* Sets the output of all 9 LEDs by passing  a byte array of colors of size 9 */
void OutputStream::setAllLEDs(byte *colorArray)
{
	// OutputStream 1 - 8
	for (uint8_t i = 0; i < 8; i++)
	{
		byte orVar = B00000001 << i;
		byte andVar = B11111111 ^ orVar;

		if (colorArray[i] & B00000001)
			outputBytes[_RED_LEDs] = outputBytes[_RED_LEDs] | orVar;
		else
			outputBytes[_RED_LEDs] = outputBytes[_RED_LEDs] & andVar;
		if (colorArray[i] & B00000010)
			outputBytes[_GREEN_LEDs] = outputBytes[_GREEN_LEDs] | orVar;
		else
			outputBytes[_GREEN_LEDs] = outputBytes[_GREEN_LEDs] & andVar;
		if (colorArray[i] & B00000100)
			outputBytes[_BLUE_LEDs] = outputBytes[_BLUE_LEDs] | orVar;
		else
			outputBytes[_BLUE_LEDs] = outputBytes[_BLUE_LEDs] & andVar;
	}

	// OutputStream 9
	if (colorArray[8] & B00000001)
		outputBytes[_LED9] = outputBytes[_LED9] | B00000001;
	else
		outputBytes[_LED9] = outputBytes[_LED9] & B11111110;
	if (colorArray[8] & B00000010)
		outputBytes[_LED9] = outputBytes[_LED9] | B00000010;
	else
		outputBytes[_LED9] = outputBytes[_LED9] & B11111101;
	if (colorArray[8] & B00000100)
		outputBytes[_LED9] = outputBytes[_LED9] | B00000100;
	else
		outputBytes[_LED9] = outputBytes[_LED9] & B11111011;

	// Send output to pins
	setOutputPins();
}

/* Sets the output of all 9 LEDs to the same color as defined by the argument */
void OutputStream::setAllLEDs(byte color)
{
	// OutputStream 1 - 8
	for (uint8_t i = 0; i < 8; i++)
	{
		byte orVar = B00000001 << i;
		byte andVar = B11111111 ^ orVar;

		if (color & B00000001)
			outputBytes[_RED_LEDs] = outputBytes[_RED_LEDs] | orVar;
		else
			outputBytes[_RED_LEDs] = outputBytes[_RED_LEDs] & andVar;
		if (color & B00000010)
			outputBytes[_GREEN_LEDs] = outputBytes[_GREEN_LEDs] | orVar;
		else
			outputBytes[_GREEN_LEDs] = outputBytes[_GREEN_LEDs] & andVar;
		if (color & B00000100)
			outputBytes[_BLUE_LEDs] = outputBytes[_BLUE_LEDs] | orVar;
		else
			outputBytes[_BLUE_LEDs] = outputBytes[_BLUE_LEDs] & andVar;
	}

	// OutputStream 9
	if (color & B00000001)
		outputBytes[_LED9] = outputBytes[_LED9] | B00000001;
	else
		outputBytes[_LED9] = outputBytes[_LED9] & B11111110;
	if (color & B00000010)
		outputBytes[_LED9] = outputBytes[_LED9] | B00000010;
	else
		outputBytes[_LED9] = outputBytes[_LED9] & B11111101;
	if (color & B00000100)
		outputBytes[_LED9] = outputBytes[_LED9] | B00000100;
	else
		outputBytes[_LED9] = outputBytes[_LED9] & B11111011;

	// Send output to pins
	setOutputPins();
}

/* Set the color of an individual OutputStream (1 - 9) */
void OutputStream::setLED(byte led, byte color)
{
	byte orVar = B00000001 << (led - 1);
	byte andVar = B11111111 ^ orVar;

	if (led <= 8)
	{
		if (color & B00000001)
			outputBytes[_RED_LEDs] = outputBytes[_RED_LEDs] | orVar;
		else
			outputBytes[_RED_LEDs] = outputBytes[_RED_LEDs] & andVar;
		if (color & B00000010)
			outputBytes[_GREEN_LEDs] = outputBytes[_GREEN_LEDs] | orVar;
		else
			outputBytes[_GREEN_LEDs] = outputBytes[_GREEN_LEDs] & andVar;
		if (color & B00000100)
			outputBytes[_BLUE_LEDs] = outputBytes[_BLUE_LEDs] | orVar;
		else
			outputBytes[_BLUE_LEDs] = outputBytes[_BLUE_LEDs] & andVar;
	}
	else if (led == 9)
	{
		if (color & B00000001)
			outputBytes[_LED9] = outputBytes[_LED9] | B00000001;
		else
			outputBytes[_LED9] = outputBytes[_LED9] & B11111110;
		if (color & B00000010)
			outputBytes[_LED9] = outputBytes[_LED9] | B00000010;
		else
			outputBytes[_LED9] = outputBytes[_LED9] & B11111101;
		if (color & B00000100)
			outputBytes[_LED9] = outputBytes[_LED9] | B00000100;
		else
			outputBytes[_LED9] = outputBytes[_LED9] & B11111011;
	}

	// Send output to pins
	setOutputPins();
}

/* CLears the output of all 9 LEDs */
void OutputStream::clearLEDs()
{
	outputBytes[_RED_LEDs] = B00000000;
	outputBytes[_GREEN_LEDs] = B00000000;
	outputBytes[_BLUE_LEDs] = B00000000;
	outputBytes[_LED9] = B00000000;

	// Send output to pins
	setOutputPins();
}

/* Sets the 7-Segment Display (selected by the displayNo 1-2) with the specified character (int 0-9 and '.') */
void OutputStream::set7SegmentDisplay(uint8_t displayNo, char value)
{
	/* Note:
	  -2-
	3|	|1
	  -4-
	5|	|7
	  -6-  o
	8
	*/
	byte index;
	if (displayNo == 1)
		index = _7SEGMENT_1;
	else if (displayNo == 2)
		index = _7SEGMENT_2;
	else
		return;

	switch (value)
	{
	case 0: outputBytes[index] = B01110111;
		break;
	case 1: outputBytes[index] = B01000001;
		break;
	case 2: outputBytes[index] = B00111011;
		break;
	case 3: outputBytes[index] = B01101011;
		break;
	case 4: outputBytes[index] = B01001101;
		break;
	case 5: outputBytes[index] = B01101110;
		break;
	case 6: outputBytes[index] = B01111110;
		break;
	case 7: outputBytes[index] = B01000011;
		break;
	case 8: outputBytes[index] = B01111111;
		break;
	case 9: outputBytes[index] = B01101111;
		break;
	case 'a': outputBytes[index] = B00000001;
		break;
	case 'b': outputBytes[index] = B01000000;
		break;
	case 'c': outputBytes[index] = B00100000;
		break;
	case 'd': outputBytes[index] = B00010000;
		break;
	case 'e': outputBytes[index] = B00000100;
		break;
	case 'f': outputBytes[index] = B00000010;
		break;
	case 'g': outputBytes[index] = B00000001;
		break;
	case '.': outputBytes[index] = B10000000;
		break;
	case 'L': outputBytes[index] = B00110100;
		break;
	}

	// Send output to pins
	setOutputPins();
}

void OutputStream::showButtonStatus(int button, byte color)
{
	Serial.println(button, BIN);
	for (uint8_t i = 0; i < 9; i++)
	{
		int iterator = 1 << i;
		if (button & iterator)
			setLED(i+1, color);
		else
			setLED(i+1, NULL);
	}
}

/* Clears the 7Segment Display specified by the displayNo 1-2 */
void OutputStream::clear7SegmentDisplay(uint8_t displayNo)
{
	byte index;
	if (displayNo == 1)
		index = _7SEGMENT_1;
	else if (displayNo == 2)
		index = _7SEGMENT_2;
	else
		return;

	outputBytes[index] = B00000000;

	// Send output to pins
	setOutputPins();
}