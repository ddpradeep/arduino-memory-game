// 
// 
// 

#include "OS.h"

void OS::init()
{
	for (uint8_t i = 0; i < MAX_PROCESS_SIZE; i++)
	{
		processList[i] = NULL_ADDRESS;
		functionPtrs[i] = 0;
		intervalList[i] = NULL_ADDRESS;
	}

	processCount = 0;
	emptyPtr = 0;
}

void OS::findNextEmptyLocation()
{
	for (uint8_t i = 0; i < MAX_PROCESS_SIZE; i++)
	{
		if (processList[i] == NULL_ADDRESS)
		{
			emptyPtr = i;
			break;
		}
	}
}

int8_t OS::every(unsigned long period, void(*callback)())
{
	if (processCount >= MAX_PROCESS_SIZE)
		return -1;
	findNextEmptyLocation();
	functionPtrs[emptyPtr] = callback;
	processList[emptyPtr] = timer.every(period, callback);
	intervalList[emptyPtr] = period;
	/*Serial.print("Empty Address: ");
	Serial.println(emptyPtr);
	Serial.print("Process Launched with id ");
	Serial.println(processList[emptyPtr]);*/
	processCount++;
	return processList[emptyPtr];
}

boolean OS::stop(int8_t id)
{
	if (id == NULL_ADDRESS)
		return false;
	for (uint8_t i = 0; i < MAX_PROCESS_SIZE; i++)
		if (processList[i] == id)
		{
			/*Serial.println("Stopped Process");*/
			timer.stop(processList[i]);
			processList[i] = NULL_ADDRESS;
			processCount--;
			return true;
		}
		/*Serial.println("Failed to Stop");*/
	return false;
}

boolean OS::stop(void(*callback)())
{
	for (uint8_t i = 0; i < MAX_PROCESS_SIZE; i++)
	{
		if (functionPtrs[i] == callback)
		{
			/*Serial.print ("Trying to  Stop");
			Serial.print(" ");
			Serial.println(i);*/
			stop(processList[i]);
			return true;
		}
	}
	return false;
}

void OS::stopAll()
{
	if (!processCount)
		return;
	for (uint8_t i = 0; i < MAX_PROCESS_SIZE; i++)
	{
		if (processList[i])
			stop(processList[i]);
	}
	init();
}

void OS::delay(void(*callback)(), int milliseconds)
{
	int interval = 0;
	/*Serial.println("Attempting delay call");*/
	for (uint8_t i = 0; i < MAX_PROCESS_SIZE; i++)
	{
		if (functionPtrs[i] == callback)
		{
			/*Serial.print ("Trying to  Stop");
			Serial.print(" ");
			Serial.println(i);*/
			stop(processList[i]);
			interval = intervalList[i];
		}
	}
	unsigned long targetTime = millis() + milliseconds;
	while (millis() < targetTime)
	{
		timer.update();
	}
	if (interval)
		every(interval, callback);
}

void OS::update()
{
	timer.update();
}

int OS::availableMemory() {
	int size = 1024; // Use 2048 with ATmega328
	byte *buf;

	while ((buf = (byte *)malloc(--size)) == NULL)
		;

	free(buf);

	return size;
}

