// OutputStream.h

#ifndef _LED_h
#define _LED_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "Arduino.h"
#else
	#include "WProgram.h"
#endif

// Constants
#define RED				(B00000001)	// List of Color Modes supported by OutputStream
#define GREEN			(B00000010)	// List of Color Modes supported by OutputStream
#define BLUE			(B00000100)	// List of Color Modes supported by OutputStream
#define PURPLE			(B00000101)	// List of Color Modes supported by OutputStream
#define YELLOW			(B00000011)	// List of Color Modes supported by OutputStream
#define CYAN			(B00000110)	// List of Color Modes supported by OutputStream
#define WHITE			(B00000111)	// List of Color Modes supported by OutputStream
#define NULL			(B00000000)	// List of Color Modes supported by OutputStream

#define DISPLAY1		(1)			// 7Segment Display 1
#define DISPLAY2		(2)			// 7Segment Display 2

#define _7SEGMENT_1		(0)		// To Address Indexes of OutputState (See Below)
#define _7SEGMENT_2		(1)		// To Address Indexes of OutputState (See Below)
#define _LED9			(2)		// To Address Indexes of OutputState (See Below)
#define _BLUE_LEDs		(3)		// To Address Indexes of OutputState (See Below)
#define _GREEN_LEDs		(4)		// To Address Indexes of OutputState (See Below)
#define _RED_LEDs		(5)		// To Address Indexes of OutputState (See Below)

class OutputStream
{
 private:
	 uint8_t latchPin;			// ShiftOut Pins
	 uint8_t clockPin;			// ShiftOut Pins
	 uint8_t dataPin;			// ShiftOut Pins

	 byte outputBytes[6];		// Placeholders for 8bRED, 8bGREEN, 8bBLUE, 8bRemainder, 2x 8b7SegmentIC chips

	 void shiftOut(byte data);
	 void setOutputPins();

 public:
	 void init(uint8_t latchPin, uint8_t clockPin, uint8_t dataPin);
	 void reset();

	 void setAllLEDs(byte *colors);
	 void setAllLEDs(byte color);
	 void setLED(byte led, byte color);
	 void showButtonStatus(int button, byte color);
	 void clearLEDs();

	 void set7SegmentDisplay(uint8_t displayNo, char value);
	 void clear7SegmentDisplay(uint8_t displayNo);
};
#endif

