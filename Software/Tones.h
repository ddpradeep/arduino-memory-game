// Tones.h

#ifndef _TONES_h
#define _TONES_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "Arduino.h"
#else
	#include "WProgram.h"
#endif

/*************************************************
* Public Constants
*************************************************/

#define __B0  31
#define __C1  33
#define __CS1 35
#define __D1  37
#define __DS1 39
#define __E1  41
#define __F1  44
#define __FS1 46
#define __G1  49
#define __GS1 52
#define __A1  55
#define __AS1 58
#define __B1  62
#define __C2  65
#define __CS2 69
#define __D2  73
#define __DS2 78
#define __E2  82
#define __F2  87
#define __FS2 93
#define __G2  98
#define __GS2 104
#define __A2  110
#define __AS2 117
#define __B2  123
#define __C3  131
#define __CS3 139
#define __D3  147
#define __DS3 156
#define __E3  165
#define __F3  175
#define __FS3 185
#define __G3  196
#define __GS3 208
#define __A3  220
#define __AS3 233
#define __B3  247
#define __C4  262
#define __CS4 277
#define __D4  294
#define __DS4 311
#define __E4  330
#define __F4  349
#define __FS4 370
#define __G4  392
#define __GS4 415
#define __A4  440
#define __AS4 466
#define __B4  494
#define __C5  523
#define __CS5 554
#define __D5  587
#define __DS5 622
#define __E5  659
#define __F5  698
#define __FS5 740
#define __G5  784
#define __GS5 831
#define __A5  880
#define __AS5 932
#define __B5  988
#define __C6  1047
#define __CS6 1109
#define __D6  1175
#define __DS6 1245
#define __E6  1319
#define __F6  1397
#define __FS6 1480
#define __G6  1568
#define __GS6 1661
#define __A6  1760
#define __AS6 1865
#define __B6  1976
#define __C7  2093
#define __CS7 2217
#define __D7  2349
#define __DS7 2489
#define __E7  2637
#define __F7  2794
#define __FS7 2960
#define __G7  3136
#define __GS7 3322
#define __A7  3520
#define __AS7 3729
#define __B7  3951
#define __C8  4186
#define __CS8 4435
#define __D8  4699
#define __DS8 4978
#define BUTTON_1_TONE	__C6
#define BUTTON_2_TONE	__D6
#define BUTTON_3_TONE	__E6
#define BUTTON_4_TONE	__F6
#define BUTTON_5_TONE	__G6
#define BUTTON_6_TONE	__GS6
#define BUTTON_7_TONE	__A6
#define BUTTON_8_TONE	__AS6
#define BUTTON_9_TONE	__B6


#endif

