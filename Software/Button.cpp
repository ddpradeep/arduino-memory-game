// 
// 
// 

#include "Button.h"

void Button::init()
{
	pinMode(BUTTON1, INPUT);
	pinMode(BUTTON2, INPUT);
	pinMode(BUTTON3, INPUT);
	pinMode(BUTTON4, INPUT);
	pinMode(BUTTON5, INPUT);
	pinMode(BUTTON6, INPUT);
	pinMode(BUTTON7, INPUT);
	pinMode(BUTTON8, INPUT);
	pinMode(BUTTON9, INPUT);

	lastButtonState = 0;
}

/* Returns an int value representing the on/off status of each of the 9 push buttons (LSB - Button 1) */
int Button::getAllButtonStatus()
{
	int status = 0;
	if (isButton1Pressed())
	{
		status = status | B00000001;
	}
	if (isButton2Pressed())
	{
		status = status | B00000010;
	}
	if (isButton3Pressed())
	{
		status = status | B00000100;
	}
	if (isButton4Pressed())
	{
		status = status | B00001000;
	}
	if (isButton5Pressed())
	{
		status = status | B00010000;
	}

	if (isButton6Pressed())
	{
		status = status | B00100000;
	}
	if (isButton7Pressed())
	{
		status = status | B01000000;
	}
	if (isButton8Pressed())
	{
		status = status | B10000000;
	}
	if (isButton9Pressed())
	{
		status = status + 256;
	}
	lastButtonState = status & (lastConsumedState ^ status);
	lastConsumedState = lastConsumedState & status;

	return status;
}

void Button::resetButtonBuffer()
{
	lastButtonState = 0;
}

void Button::clearLastConsumedBuffer()
{
	lastConsumedState = 0;
}

boolean Button::isAnyButtonPressed()
{
	//lastButtonState = getAllButtonStatus();
	if (lastButtonState)
	{
		lastConsumedState = lastButtonState;
		return true;
	}
	else
		return false;
}

boolean Button::isButtonPressed(uint8_t button, uint8_t mode)
{
	if (!isAnyButtonPressed())
		return false;
	int target = (int)1 << button;
	switch (mode)
	{
	case ONLY_BUTTON_TRIGGERED:
		if (lastButtonState == target)
			return true;
		else
			return false;
	case ONE_OF_THE_BUTTONS_TRIGGRED:
		if (lastButtonState & target)
			return true;
		else
			return false;
	}
}

uint8_t Button::getButtonPressed()
{
	switch (lastButtonState)
	{
	case 0:
		return NO_PRESS;
	case 1:
		return BUTTON_1;
	case 2:
		return BUTTON_2;
	case 4:
		return BUTTON_3;
	case 8:
		return BUTTON_4;
	case 16:
		return BUTTON_5;
	case 32:
		return BUTTON_6;
	case 64:
		return BUTTON_7;
	case 128:
		return BUTTON_8;
	case 256:
		return BUTTON_9;
	default:
		return MULTIPLE_PRESS;
	}
}

/* Functions to call to check if individual buttons have been pressed (No Overhead) */
inline boolean Button::isButton1Pressed()
{
	return (boolean)digitalRead(BUTTON1);
}

inline boolean Button::isButton2Pressed()
{
	return (boolean)digitalRead(BUTTON2);
}

inline boolean Button::isButton3Pressed()
{
	return (boolean)digitalRead(BUTTON3);
}

inline boolean Button::isButton4Pressed()
{
	return (boolean)digitalRead(BUTTON4);
}

inline boolean Button::isButton5Pressed()
{
	return (boolean)digitalRead(BUTTON5);
}

inline boolean Button::isButton6Pressed()
{
	return (boolean)digitalRead(BUTTON6);
}

inline boolean Button::isButton7Pressed()
{
	return (boolean)digitalRead(BUTTON7);
}

inline boolean Button::isButton8Pressed()
{
	return (boolean)digitalRead(BUTTON8);
}

inline boolean Button::isButton9Pressed()
{
	return (boolean)digitalRead(BUTTON9);
}

