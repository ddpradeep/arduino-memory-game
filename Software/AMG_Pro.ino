﻿
#include "Button.h"
#include "Tones.h"
#include "OS.h"
#include "OutputStream.h"
#include "Timer.h"

// IO Pins for Output Stream (LEDs and 7 Segment Display)
#define ST_CP	(8)			// ST_CP of 74HC595
#define SH_CP	(12)		// SH_CP of 74HC595
#define DS		(11)		// DS of 74HC595

// IO Pin for Buzzer
#define BUZZER	(7)

// IO Pins for Button
#define BUTTON_ISR_PIN (2)		// Interrupt pin for activation of any input push-buttons
#define MODECHANGE_ISR_PIN (3)	// Interrupt pin for change of game mode (toggle switch)

#define GAME_MODE_1		(1)		// Game Mode 1 (Name?)
#define GAME_MODE_2		(2)		// Game Mode 2 (Name?)
#define MAX_LEVEL		(9)

#define INTRO			(1)
#define	GENERATE_PATTERN (2)
#define DISPLAY_PATTERN	(3)
#define WAIT_FOR_INPUT	(4)
#define VERIFY_INPUT	(5)

// Global Variables

OS os;						// To handle multiple tasks (non-preemptive)
OutputStream display;		// To handle output to the LED Panel and 7 Segment Display
Button button;				// To handle input from buttons

volatile boolean buttonPressed;			// Flag for Button Pressed event

uint8_t gameMode;				// Game Mode Indicator

static uint8_t whichButton = 0;
uint8_t BUTTON_LED_EFFECT;
int BUTTON_TONES[9] = { BUTTON_1_TONE, BUTTON_2_TONE, BUTTON_3_TONE, BUTTON_4_TONE, BUTTON_5_TONE, BUTTON_6_TONE, BUTTON_7_TONE, BUTTON_8_TONE, BUTTON_9_TONE };

uint8_t TARGET_MODE;
uint8_t currentMode;

uint8_t MODE_1_STATE;
uint8_t LAST_MODE_1_STATE;
uint8_t displayPatternCount_mode1;
uint8_t sequenceLength_mode1 = 0;
uint8_t inputSequenceCount_mode1;
uint8_t level_mode1;
uint8_t correctSeq_mode1[20];
uint8_t inputSeq_mode1[20];
uint8_t diff;

uint8_t MODE_2_STATE;
uint8_t LAST_MODE_2_STATE;
uint8_t displayPatternCount_mode2;
uint8_t sequenceLength_mode2 = 0;
uint8_t inputSequenceCount_mode2;
uint8_t level_mode2;
uint8_t correctSeq_mode2[20];
uint8_t inputSeq_mode2[20];

void initISRs()
{
	// Set pinmode for Push Buttons and Switch pins
	pinMode(BUTTON_ISR_PIN, INPUT);
	pinMode(MODECHANGE_ISR_PIN, INPUT);

	// Reset Button Pressed event
	buttonPressed = false;

	// Initialize ISR
	attachInterrupt(0, pushButtonISR, CHANGE);
	attachInterrupt(1, gameModeChangeISR, CHANGE);
}

void activateMode1()
{
	TARGET_MODE = GAME_MODE_1;
	MODE_1_STATE = LAST_MODE_1_STATE;
	if (MODE_1_STATE == WAIT_FOR_INPUT)
		MODE_1_STATE = GENERATE_PATTERN;
	MODE_1_STATE = INTRO;
	LAST_MODE_2_STATE = MODE_2_STATE;
	MODE_2_STATE = NULL;

	os.stop(mode_2);
	os.every(50, mode_1);
	Serial.println("Mode 1 activated");
}

void activateMode2()
{
	TARGET_MODE = GAME_MODE_2;
	MODE_2_STATE = LAST_MODE_2_STATE;
	if (MODE_2_STATE == WAIT_FOR_INPUT)
		MODE_2_STATE = GENERATE_PATTERN;
	MODE_2_STATE = INTRO;
	LAST_MODE_1_STATE = MODE_1_STATE;
	MODE_1_STATE = NULL;

	os.stop(mode_1);
	os.every(50, mode_2);
	Serial.println("Mode 2 activated");
}

void setup() {
	randomSeed(analogRead(A0));
	Serial.begin(115200);

	// Initialize Inputs and Outputs
	display.init(ST_CP, SH_CP, DS);
	os.init();
	button.init();
	initISRs();

	gameMode = digitalRead(MODECHANGE_ISR_PIN) + 1;
	BUTTON_LED_EFFECT = false;

	os.every(40, buttonPressHandler);
	MODE_1_STATE = LAST_MODE_1_STATE = INTRO;
	MODE_2_STATE = LAST_MODE_2_STATE = INTRO;

	if (digitalRead(MODECHANGE_ISR_PIN))
	{
		activateMode1();
		currentMode = GAME_MODE_1;
	}
	else
	{
		activateMode2();
		currentMode = GAME_MODE_2;
	}
}

/* **************************************************************************************************
   *                                 INTERRUPT SERVICE ROUTINES                                     *
   ************************************************************************************************** */

void pushButtonISR()
{
	// Flag button pressed event
	if (digitalRead(BUTTON_ISR_PIN))
	{
		buttonPressed = true;
		buttonPressHandler();
	}
	else
		buttonPressed = false;
}

void gameModeChangeISR()
{
	if (digitalRead(MODECHANGE_ISR_PIN))
	{
		TARGET_MODE = GAME_MODE_1;
	}
	else
	{
		TARGET_MODE = GAME_MODE_2;
	}
}

/* **************************************************************************************************
   *                                          HARDWARE API                                          *
   ************************************************************************************************** */




/* **************************************************************************************************
   *                                            MAIN LOOP                                           *
   ************************************************************************************************** */

void loop() {
	os.update();
	if (TARGET_MODE != currentMode)
	{
		noTone(BUZZER);
		Serial.println("Manual Activation");
		switch (TARGET_MODE)
		{
		case GAME_MODE_1:
			activateMode1();
			currentMode = GAME_MODE_1;
			break;
		case GAME_MODE_2:
			activateMode2();
			currentMode = GAME_MODE_2;
			break;
		}
	}
}

// Button Handler Thread (@ 50ms)
void buttonPressHandler()
{
	if (buttonPressed)
	{
		// Update the button buffer
		int buffer = button.getAllButtonStatus();
		if (BUTTON_LED_EFFECT)
		{
			display.showButtonStatus(buffer, BLUE);
			if (button.getButtonPressed())
			{
				tone(BUZZER, BUTTON_TONES[button.getButtonPressed() - 1]);
			}
		}
	}
	else
	{
		// Empty history monitoring for ignoring long press
		if (BUTTON_LED_EFFECT)
		{
			display.clearLEDs();
			noTone(BUZZER);
		}
		button.clearLastConsumedBuffer();
	}
}

/*******************************************************************************************************************************************************
* Game Modes
* Created by han, 10/01/2014
*******************************************************************************************************************************************************/

void flashingCross(void(*callback)(), int interval, uint8_t repeat)
{
	uint8_t pattern1[] =
	{
		RED, NULL, RED,
		NULL, RED, NULL,
		RED, NULL, RED
	};

	for (uint8_t i = 0; i < repeat; i++)
	{
		display.setAllLEDs(pattern1);
		tone(BUZZER, __B1);
		display.clear7SegmentDisplay(DISPLAY1);
		display.clear7SegmentDisplay(DISPLAY2);
		os.delay(callback, interval);
		display.clearLEDs();
		noTone(BUZZER);
		if (TARGET_MODE != currentMode) return;
		display.set7SegmentDisplay(DISPLAY1, 'L');
		display.set7SegmentDisplay(DISPLAY2, callback==mode_1?level_mode1:level_mode2);
		os.delay(callback, interval / 1.5);
	}
	display.clearLEDs();
	noTone(BUZZER);
}

void shiningGreenStar(void(*callback)(), int interval, uint8_t repeat)
{
	uint8_t pattern1[] =
	{
		NULL, NULL, NULL,
		NULL, GREEN, NULL,
		NULL, NULL, NULL
	};
	uint8_t pattern2[] =
	{
		NULL, GREEN, NULL,
		GREEN, GREEN, GREEN,
		NULL, GREEN, NULL
	};
	uint8_t pattern3[] =
	{
		GREEN, GREEN, GREEN,
		GREEN, GREEN, GREEN,
		GREEN, GREEN, GREEN
	};
	for (uint8_t i = 0; i < repeat; i++)
	{
		tone(BUZZER, __C6);
		os.delay(callback, interval);
		display.setAllLEDs(pattern1);
		os.delay(callback, interval);
		noTone(BUZZER);
		if (TARGET_MODE != currentMode) return;
		display.setAllLEDs(pattern2);
		os.delay(callback, interval - 10);
		tone(BUZZER, __C6);
		os.delay(callback, 10);
		display.setAllLEDs(pattern3);
		os.delay(callback, interval);
		display.setAllLEDs(pattern3);
		os.delay(callback, interval);
		display.setAllLEDs(pattern2);
		os.delay(callback, interval);
		noTone(BUZZER);
		if (TARGET_MODE != currentMode) return;
		tone(BUZZER, __E6);
		display.setAllLEDs(pattern1);
		os.delay(callback, interval);
		display.clearLEDs();
		os.delay(callback, interval);
		noTone(BUZZER);
		if (TARGET_MODE != currentMode) return;

		os.delay(callback, interval);
		tone(BUZZER, __G6);
		display.setAllLEDs(pattern1);
		os.delay(callback, interval);
		display.setAllLEDs(pattern2);
		os.delay(callback, interval);
		display.setAllLEDs(pattern3);
		os.delay(callback, interval);
		noTone(BUZZER);
		if (TARGET_MODE != currentMode) return;
		display.setAllLEDs(pattern3);
		os.delay(callback, interval);
		tone(BUZZER, __E6);
		display.setAllLEDs(pattern2);
		os.delay(callback, interval);
		display.setAllLEDs(pattern1);
		os.delay(callback, interval - 20);
		noTone(BUZZER);
		if (TARGET_MODE != currentMode) return;
		tone(BUZZER, __G6);
		os.delay(callback, 20);
		display.clearLEDs();
		os.delay(callback, interval);
	}
	display.clearLEDs();
	os.delay(callback, interval*1.5);
	noTone(BUZZER);
}

void rotatingStar(void(*callback)(), uint8_t interval, uint8_t repeat, byte COLOR1, byte COLOR2)
{
	uint8_t pattern1[] =
	{
		COLOR1, NULL, COLOR2,
		NULL, NULL, NULL,
		COLOR2, NULL, COLOR1
	};

	uint8_t pattern2[] =
	{
		NULL, COLOR1, NULL,
		COLOR2, COLOR1 | COLOR2, COLOR2,
		NULL, COLOR1, NULL
	};

	uint8_t pattern3[] =
	{
		COLOR2, NULL, COLOR1,
		NULL, NULL, NULL,
		COLOR1, NULL, COLOR2
	};

	uint8_t pattern4[] =
	{
		NULL, COLOR2, NULL,
		COLOR1, COLOR1 | COLOR2, COLOR1,
		NULL, COLOR2, NULL
	};

	for (uint8_t i = 0; i < repeat; i++)
	{
		tone(BUZZER, callback == mode_1 ? __F5 : __A6);
		//os.delay(buzzerThread, 200);

		// turn off tone function for pin 6:
		display.setAllLEDs(pattern1);
		display.set7SegmentDisplay(DISPLAY1, 'a');
		display.set7SegmentDisplay(DISPLAY2, 'g');
		os.delay(callback, interval/2);
		display.set7SegmentDisplay(DISPLAY1, 'b');
		display.set7SegmentDisplay(DISPLAY2, 'f');
		os.delay(callback, interval / 2);
		noTone(BUZZER);
		if (TARGET_MODE != currentMode) return;
		tone(BUZZER, callback == mode_1?__D5:__F5);
		display.setAllLEDs(pattern2);
		display.set7SegmentDisplay(DISPLAY1, 'c');
		display.set7SegmentDisplay(DISPLAY2, 'e');
		os.delay(callback, interval / 2);
		display.set7SegmentDisplay(DISPLAY1, 'd');
		display.set7SegmentDisplay(DISPLAY2, 'd');
		os.delay(callback, interval / 2);
		noTone(BUZZER);
		if (TARGET_MODE != currentMode) return;
		tone(BUZZER, callback == mode_1 ? __F5 : __F5);
		display.setAllLEDs(pattern3);
		display.set7SegmentDisplay(DISPLAY1, 'e');
		display.set7SegmentDisplay(DISPLAY2, 'c');
		os.delay(callback, interval / 2);
		display.set7SegmentDisplay(DISPLAY1, 'f');
		display.set7SegmentDisplay(DISPLAY2, 'b');
		os.delay(callback, interval / 2);
		noTone(BUZZER);
		if (TARGET_MODE != currentMode) return;
		tone(BUZZER, callback == mode_1 ? __A6 : __D5);
		display.setAllLEDs(pattern4);
		display.set7SegmentDisplay(DISPLAY1, 'g');
		display.set7SegmentDisplay(DISPLAY2, 'a');
		os.delay(callback, interval / 2);
		display.set7SegmentDisplay(DISPLAY1, 'd');
		display.set7SegmentDisplay(DISPLAY2, 'd');
		os.delay(callback, interval / 2);
	}
	display.clearLEDs();
	display.clear7SegmentDisplay(DISPLAY1);
	display.clear7SegmentDisplay(DISPLAY2);
	noTone(BUZZER);
	os.delay(callback, interval);
}

void loadingRevolution(void(*callback)(), int interval, uint8_t repeat, byte COLOR)
{
	uint8_t pattern1[] =
	{
		COLOR, NULL, NULL,
		COLOR, NULL, NULL,
		NULL, NULL, NULL
	};
	uint8_t pattern2[] =
	{
		COLOR, COLOR, NULL,
		NULL, NULL, NULL,
		NULL, NULL, NULL
	};
	uint8_t pattern3[] =
	{
		NULL, COLOR, COLOR,
		NULL, NULL, NULL,
		NULL, NULL, NULL
	};
	uint8_t pattern4[] =
	{
		NULL, NULL, COLOR,
		NULL, NULL, COLOR,
		NULL, NULL, NULL
	};
	uint8_t pattern5[] =
	{
		NULL, NULL, NULL,
		NULL, NULL, COLOR,
		NULL, NULL, COLOR
	};
	uint8_t pattern6[] =
	{
		NULL, NULL, NULL,
		NULL, NULL, NULL,
		NULL, COLOR, COLOR
	};
	uint8_t pattern7[] =
	{
		NULL, NULL, NULL,
		NULL, NULL, NULL,
		COLOR, COLOR, NULL
	};
	uint8_t pattern8[] =
	{
		NULL, NULL, NULL,
		COLOR, NULL, NULL,
		COLOR, NULL, NULL
	};

	for (uint8_t i = 0; i < repeat; i++)
	{
		display.setAllLEDs(pattern1);
		os.delay(callback, interval);
		display.setAllLEDs(pattern2);
		os.delay(callback, interval);
		display.setAllLEDs(pattern3);
		os.delay(callback, interval);
		display.setAllLEDs(pattern4);
		os.delay(callback, interval);
		display.setAllLEDs(pattern5);
		os.delay(callback, interval);
		display.setAllLEDs(pattern6);
		os.delay(callback, interval);
		display.setAllLEDs(pattern7);
		os.delay(callback, interval);
		display.setAllLEDs(pattern8);
		os.delay(callback, interval);
	}
	display.clearLEDs();
	noTone(BUZZER);
	os.delay(callback, interval);
}

/******************************************************************************************************************
*	Game logics
********************************************************************************************************************/

void mode_1(){
	switch (MODE_1_STATE)
	{
		case INTRO:
			display.clear7SegmentDisplay(DISPLAY1);
			display.clear7SegmentDisplay(DISPLAY2);
			BUTTON_LED_EFFECT = false;
			reset(correctSeq_mode1, 20); // clear the correctSeq_mode1 array
			rotatingStar(mode_1, 250, 2, BLUE, RED);
			display.set7SegmentDisplay(DISPLAY1, 'L');
			display.set7SegmentDisplay(DISPLAY2, level_mode1);
			if (TARGET_MODE != currentMode) return;
			os.delay(mode_1, 1000);
			MODE_1_STATE = GENERATE_PATTERN;
			level_mode1 = 1;
			break;
		case GENERATE_PATTERN:
			if (level_mode1 <= MAX_LEVEL)
			{
				displayPatternCount_mode1 = 0;
				init_sequence(level_mode1, correctSeq_mode1); // initiate a random correctSeq_mode1
				display.set7SegmentDisplay(DISPLAY1, 'L');
				display.set7SegmentDisplay(DISPLAY2, level_mode1);
				if (TARGET_MODE != currentMode) return;
				os.delay(mode_1, 500);
				MODE_1_STATE = DISPLAY_PATTERN;
			}
			else
			{
				// Print Victory Message
				// Reset
			}
			break;
		case DISPLAY_PATTERN:
			// display correctSeq_mode1
			sequenceLength_mode1 = level_mode1 + 1;
			if (correctSeq_mode1[displayPatternCount_mode1] != 0)
			{
				tone(BUZZER, BUTTON_TONES[correctSeq_mode1[displayPatternCount_mode1]-1]);
				display.setLED(correctSeq_mode1[displayPatternCount_mode1++], RED);
				os.delay(mode_1, 300);
				noTone(BUZZER);
				display.clearLEDs();
				if (TARGET_MODE != currentMode) return;
				os.delay(mode_1, 200);
			}
			else
			{
				inputSequenceCount_mode1 = 0;
				BUTTON_LED_EFFECT = true;
				MODE_1_STATE = WAIT_FOR_INPUT;
				button.resetButtonBuffer();
			}
			break;
		case WAIT_FOR_INPUT:
			if (inputSequenceCount_mode1 < sequenceLength_mode1)
			{
				// waiting for user input
				if (!button.isAnyButtonPressed())
					return;
				//Serial.println("Received a button press");

				inputSeq_mode1[inputSequenceCount_mode1++] = button.getButtonPressed();
				button.resetButtonBuffer();
			}
			else
			{
				BUTTON_LED_EFFECT = false;
				MODE_1_STATE = VERIFY_INPUT;
				os.delay(mode_1, 200);
				noTone(BUZZER);
				os.delay(mode_1, 300);
			}
			break;
		case VERIFY_INPUT:
			loadingRevolution(mode_1, 50, 2, CYAN);
			display.clearLEDs();
			for (uint8_t i = 0; i < sequenceLength_mode1; i++)
			{
				display.setLED(inputSeq_mode1[i], BLUE);
			}
			if (TARGET_MODE != currentMode) return;
			os.delay(mode_1, 800);
			if (TARGET_MODE != currentMode) return;
			uint8_t correctLength = 0;
			for (uint8_t i = 0; i < sequenceLength_mode1; i++)
			{
				if (inputSeq_mode1[i] == correctSeq_mode1[i])
				{
					tone(BUZZER, __C7);
					display.setLED(inputSeq_mode1[i], NULL);
					os.delay(mode_1, 100);
					display.setLED(inputSeq_mode1[i], GREEN);
					correctLength++;
					os.delay(mode_1, 100);
					noTone(BUZZER);
					os.delay(mode_1, 400);
				}
				else
				{
					tone(BUZZER, __B3);
					display.setLED(inputSeq_mode1[i], RED);
					os.delay(mode_1, 300);
					noTone(BUZZER);
					os.delay(mode_1, 500);
				}
				if (TARGET_MODE != currentMode) return;
			}
			if (TARGET_MODE != currentMode) return;
			if (correctLength!=sequenceLength_mode1)
			{
				flashingCross(mode_1, 500, 3);
				if (TARGET_MODE != currentMode) return;
				//os.stop(id_1);
				//os.delay(mode_1, 1000);
				display.clearLEDs();
				//clear7SegmentDisplay(_7SEGMENT_1);
				//set7SegmentDisplay(1, '1');
				MODE_1_STATE = INTRO;
				break;
			}
			else
			{
				os.delay(mode_1, 500);
				if (TARGET_MODE != currentMode) return;
				shiningGreenStar(mode_1, 60, 1);
				if (TARGET_MODE != currentMode) return;
				// To Next level_mode1 (Won)
				level_mode1++;
				MODE_1_STATE = GENERATE_PATTERN;
			}
			break;
	}
}

void mode_2(){
	switch (MODE_2_STATE)
	{
	case INTRO:
		display.clear7SegmentDisplay(DISPLAY1);
		display.clear7SegmentDisplay(DISPLAY2);
		reset(correctSeq_mode2, 20); // clear the correctSeq_mode2 array
		BUTTON_LED_EFFECT = false;
		rotatingStar(mode_2, 250, 2, YELLOW, PURPLE);
		display.set7SegmentDisplay(DISPLAY1, 'L');
		display.set7SegmentDisplay(DISPLAY2, level_mode2);
		if (TARGET_MODE != currentMode) return;
		os.delay(mode_2, 1000);
		MODE_2_STATE = GENERATE_PATTERN;
		level_mode2 = 1;
		break;
	case GENERATE_PATTERN:
		if (level_mode2 <= MAX_LEVEL)
		{
			displayPatternCount_mode2 = 0;
			init_sequence(level_mode2, correctSeq_mode2); // initiate a random correctSeq_mode2
			display.set7SegmentDisplay(DISPLAY1, 'L');
			display.set7SegmentDisplay(DISPLAY2, level_mode2);
			if (TARGET_MODE != currentMode) return;
			os.delay(mode_2, 500);
			MODE_2_STATE = DISPLAY_PATTERN;
		}
		else
		{
			// Print Victory Message
			// Reset
		}
		break;
	case DISPLAY_PATTERN:
		// display correctSeq_mode2
		sequenceLength_mode2 = level_mode2 + 1;
		if (correctSeq_mode2[displayPatternCount_mode2] != 0)
		{
			byte COLOR1;
			byte COLOR2;
			COLOR1 = getRandomColor(NULL);
			COLOR2 = getRandomColor(COLOR1);
			tone(BUZZER, BUTTON_TONES[5]);
			display.setAllLEDs(COLOR2);
			display.setLED(correctSeq_mode2[displayPatternCount_mode2++], COLOR1);

			os.delay(mode_2, 300 - level_mode2 * 15);
			noTone(BUZZER);
			display.clearLEDs();
			if (TARGET_MODE != currentMode) return;
			os.delay(mode_2, 200 - level_mode2 * 10);
		}
		else
		{
			inputSequenceCount_mode2 = 0;
			BUTTON_LED_EFFECT = true;
			MODE_2_STATE = WAIT_FOR_INPUT;
			button.resetButtonBuffer();
		}
		break;
	case WAIT_FOR_INPUT:
		if (inputSequenceCount_mode2 < sequenceLength_mode2)
		{
			// waiting for user input
			if (!button.isAnyButtonPressed())
				return;
			
			inputSeq_mode2[inputSequenceCount_mode2++] = button.getButtonPressed();
			button.resetButtonBuffer();
		}
		else
		{
			BUTTON_LED_EFFECT = false;
			MODE_2_STATE = VERIFY_INPUT;
			os.delay(mode_2, 200);
			noTone(BUZZER);
			os.delay(mode_2, 300);
		}
		break;
	case VERIFY_INPUT:
		loadingRevolution(mode_2, 50, 2, CYAN);
		display.clearLEDs();
		for (uint8_t i = 0; i < sequenceLength_mode2; i++)
		{
			display.setLED(inputSeq_mode2[i], BLUE);
		}
		if (TARGET_MODE != currentMode) return;
		os.delay(mode_2, 800);
		if (TARGET_MODE != currentMode) return;
		uint8_t correctLength = 0;
		for (uint8_t i = 0; i < sequenceLength_mode2; i++)
		{
			if (inputSeq_mode2[i] == correctSeq_mode2[i])
			{
				tone(BUZZER, __C7);
				display.setLED(inputSeq_mode2[i], NULL);
				os.delay(mode_2, 100);
				display.setLED(inputSeq_mode2[i], GREEN);
				correctLength++;
				os.delay(mode_2, 100);
				noTone(BUZZER);
				os.delay(mode_2, 400);
			}
			else
			{
				tone(BUZZER, __B3);
				display.setLED(inputSeq_mode2[i], RED);
				os.delay(mode_2, 300);
				noTone(BUZZER);
				os.delay(mode_2, 500);
			}
			if (TARGET_MODE != currentMode) return;
		}
		if (TARGET_MODE != currentMode) return;
		if (correctLength != sequenceLength_mode2)
		{
			flashingCross(mode_2, 500, 3);
			if (TARGET_MODE != currentMode) return;
			//os.stop(id_1);
			//os.delay(mode_2, 1000);
			display.clearLEDs();
			//clear7SegmentDisplay(_7SEGMENT_1);
			//set7SegmentDisplay(1, '1');
			MODE_2_STATE = INTRO;
			break;
		}
		else
		{
			os.delay(mode_2, 500);
			if (TARGET_MODE != currentMode) return;
			shiningGreenStar(mode_2, 60, 1);
			if (TARGET_MODE != currentMode) return;
			// To Next level_mode2 (Won)
			level_mode2++;
			MODE_2_STATE = GENERATE_PATTERN;
		}
		break;
	}
}

byte getRandomColor(byte Except)
{
	byte COLOR;
	COLOR = Except;
	while (COLOR == Except)
	{
		switch (random(1, 6))
		{
		case 1:
			COLOR = RED;
			break;
		case 2:
			COLOR = BLUE;
			break;
		case 3:
			COLOR = GREEN;
			break;
		case 4:
			COLOR = YELLOW;
			break;
		case 5:
			COLOR = PURPLE;
			break;
		case 6:
			COLOR = WHITE;
			break;
		}
		if (Except == GREEN && COLOR == YELLOW)
			COLOR = Except;
		else if (Except == YELLOW && COLOR == GREEN)
			COLOR = Except;

	}
	return COLOR;
}

void reset(uint8_t *array, uint8_t size) {
	memset(array, 0, size * sizeof(*array));
}

void init_sequence(uint8_t lvl, uint8_t *seq) {
	uint8_t base = 1;
	//randomSeed(millis()<<(millis()%6)+millis());
	for (uint8_t i = 0; i < lvl + base; i++){
		seq[i] = (uint8_t)random(1, 9);
	}
}