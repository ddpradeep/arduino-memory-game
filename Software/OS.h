// OS.h

#ifndef _OS_h
#define _OS_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "Arduino.h"
#else
	#include "WProgram.h"
#endif

#include "Timer.h"

#define MAX_PROCESS_SIZE	(20)		// Max allowed concurrent tasks

#define NULL_ADDRESS		(64)

class OS
{
private:
	Timer timer;
	int8_t processList[MAX_PROCESS_SIZE];
	int intervalList[MAX_PROCESS_SIZE];
	void(*functionPtrs[MAX_PROCESS_SIZE])();
	void findNextEmptyLocation();

	int8_t processCount;
	uint8_t emptyPtr;
public:
	void init();
	int8_t every(unsigned long period, void(*callback)(void));
	boolean stop(int8_t id);
	boolean stop(void(*callback)());
	void stopAll();
	void update(void);
	void delay(void(*callback)(), int milliseconds);
	int availableMemory();
};

#endif