// Button.h

#ifndef _BUTTON_h
#define _BUTTON_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "Arduino.h"
#else
	#include "WProgram.h"
#endif

#define ONLY_BUTTON_TRIGGERED			(1)
#define ONE_OF_THE_BUTTONS_TRIGGRED		(2)

#define BUTTON_1		(1)
#define BUTTON_2		(2)
#define BUTTON_3		(3)
#define BUTTON_4		(4)
#define BUTTON_5		(5)
#define BUTTON_6		(6)
#define BUTTON_7		(7)
#define BUTTON_8		(8)
#define BUTTON_9		(9)
#define NO_PRESS		(0)
#define MULTIPLE_PRESS	(255)

#define BUTTON1	(A5)
#define BUTTON2	(A4)
#define BUTTON3	(A3)
#define BUTTON4	(A2)
#define BUTTON5	(A1)
#define BUTTON6	(A0)
#define BUTTON7	(4)
#define BUTTON8	(6)
#define BUTTON9	(5)

class Button
{
 private:
	 int lastButtonState;
	 int lastConsumedState;

 public:
	void init();
	int getAllButtonStatus();
	boolean isButton1Pressed();
	boolean isButton2Pressed();
	boolean isButton3Pressed();
	boolean isButton4Pressed();
	boolean isButton5Pressed();
	boolean isButton6Pressed();
	boolean isButton7Pressed();
	boolean isButton8Pressed();
	boolean isButton9Pressed();

	boolean isButtonPressed(uint8_t button, uint8_t mode);
	void resetButtonBuffer();
	void clearLastConsumedBuffer();
	uint8_t getButtonPressed();
	boolean isAnyButtonPressed();
};

extern Button BUTTON;

#endif

